/*
 * Copyright 2015 Lasse Schuirmann
 * This source code may be redistributed, modified and used under te terms of
 * the AGPLv3 license. See http://choosealicense.com/licenses/agpl-3.0/ for more
 * information.
 *
 * This contains a bare implementation of a min heap (minimal value is the root)
 */

#include <stdlib.h>

typedef struct s_heap_node {
    int value;
    struct s_heap_node *parent;
    struct s_heap_node *left_child;
    struct s_heap_node *right_child;
} heap_node;

heap_node *heap_node_new(heap_node * parent, int value) {
    heap_node *node;
    node = (heap_node *) malloc(sizeof(heap_node));
    node->value = value;
    node->parent = parent;
    node->left_child = NULL;
    node->right_child = NULL;

    return node;
}

void *heap_node_delete(heap_node * this) {
    if (this->parent == NULL) {
        free(this);
    } else {
        if (this == this->parent->left_child) {
            this->parent->left_child = NULL;
        } else {
            this->parent->right_child = NULL;
        }
    }
}

bool heap_node_needs_sift_up(heap_node * this) {
    return this->parent != NULL && this->parent->value > this->value;
}

bool heap_node_needs_sift_down(heap_node * this) {
    return (this->left_child != NULL &&
            this->left_child->value < this->value) ||
           (this->right_child != NULL &&
            this->right_child->value < this->value);
}

/**
 * Switches values with the node above. (Simpler than actually switching
 * the nodes and repointing everything.)
 *
 * @returns: the upper (sifted) node
 */
heap_node *heap_node_sift_up(heap_node * this) {
    int tmp = this->parent->value;
    this->parent->value = this->value;
    this->value = tmp;
    return this->parent;
}

/**
 * Switches values with the node below. (Simpler than actually switching
 * the nodes and repointing everything.)
 *
 * @returns: the lower (sifted) node
 */
heap_node *heap_node_sift_down(heap_node * this) {
    int tmp = this->value;

    if (this->left_child->value < tmp) {
        this->value = this->left_child->value;
        this->left_child->value = tmp;
        return this->left_child;
    } else {
        this->value = this->right_child->value;
        this->right_child->value = tmp;
        return this->right_child;
    }
}

/**
 * Find the place to insert a new node in the children of the given root node,
 * insert the value there and return the new node.
 */
heap_node *heap_node_insert(heap_node * root, int value) {
    if (root->left_child == NULL) {
        root->left_child = heap_node_new(root, value);
    } else if (root->right_child == NULL) {
        root->right_child = heap_node_new(root, value);
    } else {

    }
}

void heap_node_print(heap_node * this) {
    if (this == NULL) {
        return;
    }
    printf("Value: %d", this->value);
    if (this->left_child != NULL) {
        printf("print left");
        printf(" | left %d", this->left_child->value);
      printf("DONE\n");
    }
    if (this->right_child != NULL) {printf("print right");
        printf(" | right %d", this->right_child->value);printf("DONE\n");
    }
    printf("\n");

    heap_node_print(this->left_child);
    heap_node_print(this->right_child);
}

/**
 * Will return NULL if not found.
 */
heap_node *heap_node_find(heap_node * this, int value) {
    if (this == NULL || this->value > value) {
        return NULL;
    } else if (this->value == value) {
        return this;
    } else {
        heap_node *node = heap_node_find(this->left_child, value);
        if (node != NULL) {
            return node;
        } else {
            return heap_node_find(this->right_child, value);
        }
    }
}


typedef struct s_heap {
    heap_node *root;
    unsigned int size;
} heap;

/**
 * Creates a heap.
 *
 * @param root: The root node (may be NULL or have children as well.)
 */
heap *heap_new() {
    heap *result;
    result = (heap *) malloc(sizeof(heap));
    result->root = NULL;
    result->size = 0;

    return result;
}

void heap_print(heap * this) {
    heap_node_print(this->root);
}

void heap_insert(heap * this, int value) {
    unsigned int node_index = ++(this->size);

    if (this->root == NULL) {
        this->root = heap_node_new(NULL, value);
    } else {
        // See http://bit.ly/1XGYZaZ
        heap_node *current_node = this->root;

        // Remove first binary digit
        int pot = 1;
        while (pot * 2 <= node_index) {
            pot *= 2;
        }
        node_index -= pot;

        // Traverse
        pot /= 2;
        while (pot > 1) {
            if (node_index >= pot) {
                current_node = current_node->right_child;
                node_index -= pot;
            } else {
                current_node = current_node->left_child;
            }
            pot /= 2;
        }

        if (node_index >= 1) {
            current_node->right_child = heap_node_new(current_node, value);
            current_node = current_node->right_child;
        } else {
            current_node->left_child = heap_node_new(current_node, value);
            current_node = current_node->left_child;
        }

        // Sift where needed
        while (heap_node_needs_sift_up(current_node)) {
            current_node = heap_node_sift_up(current_node);
        }
    }
}

/**
 * This is the "makeheap" whatever that shall be exactly...
 */
heap *heap_from_array(int values[], int size) {
    heap *h = heap_new();
    for (int i=0; i<size; i++) {
        heap_insert(h, values[i]);
    }

    return h;
}

heap *heap_random(int size) {
    heap *h = heap_new();
    srand(time(NULL));
    for (int i=0; i<size; i++) {
        heap_insert(h, rand());
    }

    return h;
}

int heap_findmin(heap * this) {
    return this->root->value;
}

/**
 * Will return 0 if node doesn't exist, 1 on success.
 */
int heap_delete(heap * this, int value) {
    heap_node *delnode = heap_node_find(this->root, value);
    if (delnode == NULL) {
        return 0;
    }

    if (delnode == this->root && this->size == 1) {
        heap_node_delete(delnode);
        this->root = NULL;
    }

    unsigned int node_index = this->size;
    // See http://bit.ly/1XGYZaZ
    heap_node *current_node = this->root;

    // Remove first binary digit
    int pot = 1;
    while (pot * 2 <= node_index) {
        pot *= 2;
    }
    node_index -= pot;

    // Traverse
    pot /= 2;
    while (pot >= 1) {
        if (node_index >= pot) {
            current_node = current_node->right_child;
            node_index -= pot;
        } else {
            current_node = current_node->left_child;
        }
        pot /= 2;
    }

    if (delnode == current_node) {
        heap_node_delete(current_node);
    } else {
        delnode->value = current_node->value;
        heap_node_delete(current_node);

        // Sift where needed
        while (heap_node_needs_sift_down(delnode)) {
            delnode = heap_node_sift_down(delnode);
        }
    }

    (this->size)--;
    return 1;
}

/**
 * Delete the minimum. Return 1 on success, 0 otherwise.
 */
int heap_deletemin(heap *this) {
    if (this == NULL) {
        printf("Not found");
        return 0;
    }

    return heap_delete(this, this->root->value);
}

heap *heap_meld(heap *this, heap *other) {
    while (other->root != NULL) {
        heap_insert(this, other->root->value);
        heap_deletemin(other);
    }
}

void heap_destroy(heap *this) {
    printf("si %d", this->size);
    for (int i=this->size; i>0; i--) {
      printf("deleting %d\n", this->root->value);
        heap_delete(this, this->root->value);
      printf("DONE\n");
    }
                                printf("PRINGINT");
    heap_print(this);
    printf("FREEING");

    free(this);
    printf(" DONE!\n");
}
