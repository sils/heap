/*
 * Copyright 2015 Lasse Schuirmann
 * This source code may be redistributed, modified and used under te terms of
 * the AGPLv3 license. See http://choosealicense.com/licenses/agpl-3.0/ for more
 * information.
 */
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<stdbool.h>

#include "heap.h"


int main(void) {
    clock_t start, end;
    int runtime;

    start = clock();
    heap *h = heap_random(100000);
    end = clock();
    runtime = 1000 * (end - start) / CLOCKS_PER_SEC;
    printf("RANDOM HEAP CREATION TAKES %d ms\n", runtime);

    heap *h2 = heap_random(100000);
    start = clock();
    heap_meld(h, h2);
    end = clock();

    return 0;
}
