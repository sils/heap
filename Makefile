# Copyright 2015 Lasse Schuirmann
# This source code may be redistributed, modified and used under te terms of
# the AGPLv3 license. See http://choosealicense.com/licenses/agpl-3.0/ for more
# information.

default: compile

compile:
	gcc -g main.c -o main

check:
	coala

run: compile
	./main
